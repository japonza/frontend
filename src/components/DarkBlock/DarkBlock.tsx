import React from "react";
import Image from "next/image";

const DarkBlock = ({ img, title, text }) => {
  return (
    <div className={"main-black-block w-100"}>
      <div className="d-flex flex-column align-items-center">
        <img
          className={"main-black-block-icon"}
          src={img}
          height={73}
          width={134}
        />
        <div className="main-black-block-title">{title}</div>
        <div className="main-black-block-text">{text}</div>
      </div>
    </div>
  );
};

export default DarkBlock;
