import React from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";

const Card = ({
  descriptionBlock,
  index,
  img,
  priceTextBlock,
  menu,
  id,
  weight,
  fire,
}) => {
  const { push } = useRouter();
  const renderLineColor = () => {
    if (index) {
      return "linear-gradient(270deg, #FFFEFE -1.67%, #FFFFFF 84.25%)";
    }
    return "linear-gradient(270deg, #FFFEFE -1.67%, #FFFFFF 84.25%)";
  };
  const goPage = () => {
    if (menu) {
      push(`/catalogDetail/${id}`).then(() => window.scrollTo(0, 0));
    }
  };

  return (
    <div
      onClick={goPage}
      className={`card-custom d-flex flex-column justify-content-between ${
        menu
          ? "col-lg-4 col-md-6 col-sm-6 cursor-pointer"
          : "col-lg-3 col-md-6 col-sm-6"
      }`}
    >
      {fire && (
        <div className={"fire"}>
          <Image src={"/img/fire.svg"} width={41} height={41} />
        </div>
      )}
      <div>
        <div
          className="card-custom-image"
          style={{
            backgroundImage: `url(${img})`,
          }}
        />

        <div
          className="card-custom-line"
          style={{
            background: renderLineColor(),
          }}
        />
      </div>
      <div className={"d-flex justify-content-between flex-column h-100"}>
        {descriptionBlock && (
          <div className="card-custom-title">{descriptionBlock.title}</div>
        )}
        {descriptionBlock && (
          <div className="card-custom-text h-100">{descriptionBlock.text}</div>
        )}
        {weight && <div className="card-custom-text">{`${weight} г`}</div>}
        <div className="card-custom-price">{`${priceTextBlock} ${
          !menu ? "Р" : ""
        }`}</div>
      </div>
    </div>
  );
};

export default Card;
