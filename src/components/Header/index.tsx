import React, { FC } from "react";
import Image from "next/image";
import NavBar from "@components/NavBar/NavBar";
import { useBurger } from "../../helpers/burgerContext";
import Hamburger from "hamburger-react";
import Link from "next/link";

type HeaderProps = {};

const Header: FC<HeaderProps> = () => {
  const { toggle, open } = useBurger();
  return (
    <div className={`header-main `}>
      <div className="container d-flex justify-content-between align-items-center">
        <Link href={"/"}>
          <Image
            className={"cursor-pointer"}
            src={"/img/footer-logo.svg"}
            width={200}
            height={50}
          />
        </Link>

        <NavBar />
        <Link href={"tel: 33-02-33"}>
          <div className="d-flex cursor-pointer header-numberPhone">
            <Image
              className={"header-main-phone-icon"}
              src={"/img/red-phone.svg"}
              width={30}
              height={30}
            />
            <div className={"header-main-phone ml-3 "}>33-02-33</div>
          </div>
        </Link>

        <div className={"burger-btn"} onClick={toggle}>
          <Hamburger color={"red"} toggled={open} />
        </div>
      </div>
    </div>
  );
};

export default Header;
