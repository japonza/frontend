import React from "react";
import { api } from "../../constant/api";
import Image from "next/image";
import NavBar from "@components/NavBar/NavBar";
import Link from "next/link";
import { useBurger } from "../../helpers/burgerContext";
import Hamburger from "hamburger-react";

const MainBanner = ({ bannerUrl, phoneNumber }) => {
  const { toggle, open } = useBurger();
  return (
    <div
      className={"main-banner"}
      style={{
        backgroundImage: `url(${bannerUrl})`,
      }}
    >
      <div className="main-banner-header-container">
        <div className="container d-flex align-items-center justify-content-md-between justify-content-around">
          <Link href={`tel: ${phoneNumber}`}>
            <div className="d-flex align-items-center">
              <Image
                src={"/img/red-phone.svg"}
                height={30}
                width={30}
                alt={"logo"}
              />
              <div className="main-banner-phone">{phoneNumber}</div>
            </div>
          </Link>

          <NavBar />
          <span className={"main-banner-city d-sm-block d-none"}>г.Пенза</span>
          <div className={"burger-btn"} onClick={toggle}>
            <Hamburger color={"red"} toggled={open} />
          </div>
        </div>
      </div>
      <div className="d-flex flex-column ">
        <div className={"main-banner-logo"}>
          <div className="container">
            <div className="d-flex justify-content-end">
              <Image src={"/img/Logo.svg"} width={824} height={204} />
            </div>
          </div>
        </div>
        <div className={"main-banner-logo mt-5"}>
          <div className="container text-right">
            <h1 className={"main-banner-logo-text"}>ДОСТАВКА ЯПОНСКОЙ КУХНИ</h1>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <Link href={"/menu"}>
          <div className="main-banner-btn reb-btn-link">Меню</div>
        </Link>
      </div>
    </div>
  );
};

export default MainBanner;
