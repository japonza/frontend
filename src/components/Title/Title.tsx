import React from "react";

const Title = ({ text }) => {
  return <div className={"main-title"}>{text}</div>;
};

export default Title;
