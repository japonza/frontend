import React from "react";
import Link from "next/link";
import { navConfig } from "./NavBar.config";
import { useRouter } from "next/router";

const NavBar = () => {
  const { route } = useRouter();
  console.log(route, "route");
  return (
    <ul className={"navBar list-unstyled m-0"}>
      {navConfig.map((item) => {
        return (
          <li key={item.id}>
            <Link href={item.path}>
              <a
                className={`navBar-item ${route === item.path ? "active" : ""}`}
              >
                {item.name}
              </a>
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default NavBar;
