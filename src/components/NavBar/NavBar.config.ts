export const navConfig = [
  {
    id: 1,
    path: "/menu",
    name: "Меню",
  },
  {
    id: 3,
    path: "/",
    name: "Главная",
  },
  {
    id: 2,
    path: "/order",
    name: "Доставка",
  },

  {
    id: 4,
    path: "/action",
    name: "Акции",
  },
];
