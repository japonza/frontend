import React from "react";

export const Banner = ({ imgPath, title, mb = "" }) => {
  return (
    <div
      className={`banner ${mb ? mb : ""}`}
      style={{
        backgroundImage: `url(${imgPath})`,
      }}
    >
      <div className={"banner-title"}>{title}</div>
    </div>
  );
};
