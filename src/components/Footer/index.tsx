import React, { useEffect, useState } from "react";
import Image from "next/image";
import { navConfig } from "@components/NavBar/NavBar.config";
import Link from "next/link";
import { fetcher } from "../../helpers/fether";
import { api } from "../../constant/api";
import { useRouter } from "next/router";

const Footer = ({ blackBg }) => {
  const [footerData, setFooterData] = useState(null);

  const { route } = useRouter();

  useEffect(() => {
    fetcher(`${api}/footer`).then((res) => {
      setFooterData(res);
    });
  }, []);

  if (!footerData) {
    return null;
  }
  // console.log(footerData);
  return (
    <>
      <div
        className="footer"
        style={{
          backgroundColor: blackBg ? "black" : "#252525",
        }}
      >
        <div className="container">
          <div className="d-flex justify-content-between align-items-center mb-5 flex-wrap">
            <div className="footer-logo">
              <Image src={"/img/footer-logo.svg"} width={200} height={72} />
            </div>
            <div className="footer-menu d-sm-block d-none">
              {navConfig.map((item) => {
                return (
                  <Link key={item.id} href={item.path}>
                    <a
                      className={`footer-link ${
                        route === item.path ? "active" : ""
                      }`}
                    >
                      {item.name}
                    </a>
                  </Link>
                );
              })}
            </div>
          </div>
          <div className="d-flex justify-content-between  flex-wrap ">
            <Link href={`tel: ${footerData.number}`}>
              <div className="footer-phone d-flex mb-3 mb-md-0 cursor-pointer">
                {footerData.prefixNumber && (
                  <span>{`8 (${footerData.prefixNumber}) `}</span>
                )}
                {footerData.number && (
                  <span className={"red"}>{footerData.number}</span>
                )}
              </div>
            </Link>
            <div className="d-flex flex-column flex-sm-row mb-3 mb-md-0">
              {footerData.workTime && (
                <div className="d-flex flex-column  mb-3 mr-md-5">
                  <span className={"footer-title mb-2 mb-md-3"}>
                    Прием заказов с доставкой:
                  </span>
                  <span className={"footer-text text-left"}>
                    {footerData.workTime}
                  </span>
                </div>
              )}
              {footerData.number && (
                <div className="d-flex flex-column  mr-5">
                  <span className={"footer-title mb-3"}>Телефон</span>
                  <Link href={`tel: ${footerData.number}`}>
                    <span className={"footer-text text-left cursor-pointer"}>
                      {footerData.number}
                    </span>
                  </Link>
                </div>
              )}

              <div className="d-flex flex-column align-items-start align-items-sm-center">
                <span className={"footer-title mb-3"}>Соц. сети</span>
                {footerData.instagramm && (
                  <Link href={footerData.instagramm}>
                    <Image
                      src={"/img/inst.svg"}
                      className={"text-left"}
                      height={44}
                      width={44}
                    />
                  </Link>
                )}
              </div>
            </div>
          </div>
          <div className="d-flex">
            <span className={"footer-text"}>
              Официальный сайт доставки <br /> японской кухни “Японза”
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
