import React from "react";
import Footer from "@components/Footer";
import { useRouter } from "next/router";

import { Burger } from "@components/Burger/Burger";
import { Head } from "next/document";

const Layout = ({ children }) => {
  const { route } = useRouter();

  return (
    <div>
      <Burger />
      <main>
        {children}
        <Footer blackBg={route === "/" || route === "/order"} />
      </main>
    </div>
  );
};

export default Layout;
