import React from "react";
import Hamburger from "hamburger-react";
import Image from "next/image";
import { navConfig } from "@components/NavBar/NavBar.config";
import Link from "next/link";
import { useRouter } from "next/router";
import { useBurger } from "../../helpers/burgerContext";

export const Burger = () => {
  const { route, push } = useRouter();

  const { open, toggle } = useBurger();
  const goPage = (path) => {
    push(path).then(() => toggle());
  };
  return (
    <div className={`burger ${open ? "active" : ""}`}>
      <div
        style={{
          position: "absolute",
          right: "20px",
          top: "20px",
          height: "50px",
          width: "50px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Hamburger color={"red"} toggled={open} toggle={toggle} />
      </div>
      <div className="d-flex justify-content-center">
        <Image src={"/img/Logo.svg"} width={200} height={50} />
      </div>
      <div className="line-white" />
      {navConfig.map((item) => {
        return (
          <div
            key={item.id}
            className={`burger-item ${route === item.path ? "active" : ""}`}
            onClick={() => goPage(item.path)}
          >
            {item.name}
          </div>
        );
      })}
      <div className="line-white" />
      <Link href={"tel: 33-02-33"}>
        <div className="d-flex align-items-center justify-content-center">
          <Image
            className={"header-main-phone-icon d-block"}
            src={"/img/red-phone.svg"}
            width={30}
            height={30}
          />
          <div className={"header-main-phone ml-3 d-block"}>33-02-33</div>
        </div>
      </Link>
    </div>
  );
};
