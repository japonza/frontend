export const addLocalStorage = (id) => {
    const oldData = JSON.parse(localStorage.getItem("favorite"))

    localStorage.setItem("favorite", JSON.stringify([...(oldData ||[]).filter(el => el !== id), id]))
}
