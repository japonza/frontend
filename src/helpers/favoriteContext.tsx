import React, {FC, useContext, useEffect, useLayoutEffect, useState} from "react";

type Props = {
    data: number[], addItemToFavoriteStore: (id: number) => void,  deleteItemFromFavorite: (id: number) => void,  setDataFunc: (data: number[]) => void,
}

export const ManageFavorite: React.Context<Props> = React.createContext({
    data: [],
    addItemToFavoriteStore: (id: number) => {},
    deleteItemFromFavorite: (id: number) => {},
    setDataFunc: (data: number[]) => {},
});

export const useFavorite = () => useContext<Props>(ManageFavorite);

export const FavoriteContext:FC<{data: string[]}> = ({children}) => {
    const [data, setData] = useState([])


    const addItemToFavoriteStore = (id: number) => {
        const data = JSON.parse(localStorage.getItem("favorite"))
        let newData = []
        if (data) {
            newData =  data.filter((el, index) => data.indexOf(el) === index)
        }
        newData.push(id)
        setData(newData)
        localStorage.setItem("favorite", JSON.stringify(newData))
    }

    const setDataFunc = (data) => {
        setData(data)
    }
    const deleteItemFromFavorite = (id) => {
        const data = JSON.parse(localStorage.getItem("favorite"))
        let newData = []
        if (data) {
            newData =  data.filter((el) => el !== id)
        }
        setData(newData)
        localStorage.setItem("favorite", JSON.stringify(newData))
    }


    return <ManageFavorite.Provider value={{data: data, addItemToFavoriteStore, deleteItemFromFavorite, setDataFunc }}>{children}</ManageFavorite.Provider>
}
