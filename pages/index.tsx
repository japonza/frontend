import React, { FC } from "react";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import MainBanner from "@components/MainBanner/MainBanner";
import Title from "@components/Title/Title";
import DarkBlock from "@components/DarkBlock/DarkBlock";
import Link from "next/link";

type MainProps = {
  mainPageData: {
    paymentBlock: {
      title: string;
      dataWithIcon: {
        id: string;
        description: string;
        icon: { url: string };
        title: string;
      }[];
    };
    gettingOrder: {
      title: string;
      dataWithIcon: {
        id: string;
        description: string;
        icon: { url: string };
        title: string;
      }[];
    };
    aboutTextText: string;
    aboutTitle: string;
    workTime: string;
    orderTitle: string;
    orderTitleCond: string;
    conditionalMainPage: {
      id: number;
      title: string;
      smallText: string;
      discription: string;
    }[];
    orderBg: { url: string };
    orderBgCond: { url: string };
    mainBanner: { url: string };
    numberField: {
      prefix: string;
      number: string;
    };
  };
};

const Main: FC<MainProps> = ({ mainPageData }) => {
  return (
    <div>
      <MainBanner
        phoneNumber={"8 (8412) 33-02-33"}
        bannerUrl={
          `${api}${
            mainPageData.mainBanner ? mainPageData.mainBanner.url : ""
          }` || "/img/banner.png"
        }
      />
      <div className="main-gray-bg">
        <div className="container">
          <Title text={mainPageData.aboutTitle} />
        </div>
        <div className="main-title-line" />
        <div className="container mt-5  ">
          <div
            dangerouslySetInnerHTML={{ __html: mainPageData.aboutTextText }}
            className="main-text"
          />
        </div>
      </div>
      <div
        className="main-order-banner"
        style={{
          backgroundImage: `url(${api}${mainPageData.orderBg.url})`,
        }}
      >
        <div className="container">
          <Title text={mainPageData.orderTitle} />
        </div>
        <div className="main-title-line" />
        <div className="container">
          <div className="d-flex flex-column mt-5 mb-5">
            <div className="main-order-title">ПО ТЕЛЕФОНУ:</div>
            <Link
              href={`tel:${mainPageData.numberField.prefix} ${mainPageData.numberField.number}`}
            >
              <div className="main-text number-link cursor-pointer">
                <span>{mainPageData.numberField.prefix}</span>
                <span className={"red ml-2"}>
                  {mainPageData.numberField.number}
                </span>
              </div>
            </Link>
          </div>
          <div className="d-flex flex-column">
            <div className="main-order-title">ЧАСЫ РАБОТЫ ДОСТАВКИ:</div>
            <div className="main-text">{mainPageData.workTime}</div>
          </div>
        </div>
      </div>
      <div className="main-gray-bg">
        <div className="container">
          <Title text={mainPageData.gettingOrder.title} />
        </div>
        <div className="main-title-line" />
        <div className="container mt-5 d-flex justify-content-xl-between justify-content-center  flex-wrap">
          {mainPageData.gettingOrder.dataWithIcon.map((item) => {
            return (
              <DarkBlock
                key={item.id}
                img={`${api}${item.icon.url}`}
                text={item.description}
                title={item.title}
              />
            );
          })}
        </div>
      </div>
      <div
        className="main-order"
        style={{
          backgroundImage: `url(${api}${mainPageData.orderBgCond.url})`,
        }}
      >
        <div className="container">
          <Title text={mainPageData.orderTitleCond} />
        </div>
        <div className="main-title-line" />
        {mainPageData.conditionalMainPage.map((item) => {
          return (
            <div key={item.id} className="container mt-5">
              {item.title && (
                <div className="main-order-reb-bg mb-5">
                  <div className="main-order-title">{item.title}</div>
                  <div className="main-order-title-red-line" />
                </div>
              )}

              <div className="main-text max-width mb-4">
                {item.discription || ""}
              </div>
              <div className="main-text-sm">{item.smallText || ""}</div>
            </div>
          );
        })}
      </div>
      <div className="main-gray-bg">
        <div className="container">
          <Title text={mainPageData.paymentBlock.title} />
        </div>
        <div className="main-title-line" />
        <div className="container mt-5 d-flex flex-column justify-content-between">
          <div className="d-flex justify-content-lg-between justify-content-center flex-wrap">
            {mainPageData.paymentBlock.dataWithIcon.map((item) => {
              return (
                <DarkBlock
                  key={item.id}
                  img={`${api}${item.icon.url}`}
                  text={item.description}
                  title={item.title}
                />
              );
            })}
          </div>

          <div className="d-flex justify-content-center mt-5">
            <Link href={"/menu"}>
              <div className="main-banner-btn font-30 reb-btn-link">
                Посмотреть меню
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const mainPageData = await fetcher(`${api}/main-page`);
  return {
    props: {
      mainPageData,
    },
  };
}

export default Main;
