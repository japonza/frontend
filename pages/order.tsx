import React, { FC } from "react";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import MainBanner from "@components/MainBanner/MainBanner";
import Title from "@components/Title/Title";
import DarkBlock from "@components/DarkBlock/DarkBlock";
import Link from "next/link";
import Header from "@components/Header";
import { Banner } from "@components/Banner/Banner";

type MainProps = {
  orderDataPage: {
    title: string;
    bg: { url: string };
  };
  mainPageData: {
    paymentBlock: {
      title: string;
      dataWithIcon: {
        id: string;
        description: string;
        icon: { url: string };
        title: string;
      }[];
    };
    gettingOrder: {
      title: string;
      dataWithIcon: {
        id: string;
        description: string;
        icon: { url: string };
        title: string;
      }[];
    };
    aboutTextText: string;
    aboutTitle: string;
    workTime: string;
    orderTitle: string;
    orderTitleCond: string;
    conditionalMainPage: {
      id: number;
      title: string;
      smallText: string;
      discription: string;
    }[];
    orderBg: { url: string };
    orderBgCond: { url: string };
    mainBanner: { url: string };
    numberField: {
      prefix: string;
      number: string;
    };
  };
};

const Order: FC<MainProps> = ({ mainPageData, orderDataPage }) => {
  console.log(mainPageData, "mainPageData");
  return (
    <div>
      <Header />
      <Banner
        mb={"mb-0"}
        title={orderDataPage.title}
        imgPath={`${api}${orderDataPage.bg.url}`}
      />
      <div className="main-gray-bg">
        <div className="container">
          <Title text={mainPageData.gettingOrder.title} />
        </div>
        <div className="main-title-line" />
        <div className="container mt-5 d-flex justify-content-sm-between flex-wrap justify-content-center">
          {mainPageData.gettingOrder.dataWithIcon.map((item) => {
            return (
              <DarkBlock
                key={item.id}
                img={`${api}${item.icon.url}`}
                text={item.description}
                title={item.title}
              />
            );
          })}
        </div>
      </div>
      <div
        className="main-order"
        style={{
          backgroundImage: `url(${api}${mainPageData.orderBgCond.url})`,
        }}
      >
        <div className="container">
          <Title text={mainPageData.orderTitleCond} />
        </div>
        <div className="main-title-line" />
        {mainPageData.conditionalMainPage.map((item) => {
          return (
            <div key={item.id} className="container mt-5">
              {item.title && (
                <div className="main-order-reb-bg mb-5">
                  <div className="main-order-title">{item.title}</div>
                  <div className="main-order-title-red-line" />
                </div>
              )}

              <div className="main-text max-width mb-4">
                {item.discription || ""}
              </div>
              <div className="main-text-sm">{item.smallText || ""}</div>
            </div>
          );
        })}
      </div>
      <div className="main-gray-bg">
        <div className="container">
          <Title text={mainPageData.paymentBlock.title} />
        </div>
        <div className="main-title-line" />
        <div className="container mt-5 d-flex flex-column justify-content-between">
          <div className="d-flex justify-content-sm-between flex-wrap justify-content-center">
            {mainPageData.paymentBlock.dataWithIcon.map((item) => {
              return (
                <DarkBlock
                  key={item.id}
                  img={`${api}${item.icon.url}`}
                  text={item.description}
                  title={item.title}
                />
              );
            })}
          </div>

          <div className="d-flex justify-content-center mt-5">
            <Link href={"/menu"}>
              <div className="main-banner-btn font-30 reb-btn-link">
                Сделать заказ
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const mainPageData = await fetcher(`${api}/main-page`);
  const orderDataPage = await fetcher(`${api}/order-page`);
  return {
    props: {
      mainPageData,
      orderDataPage,
    },
  };
}

export default Order;
