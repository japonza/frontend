import React, { FC } from "react";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import MainBanner from "@components/MainBanner/MainBanner";
import Title from "@components/Title/Title";
import DarkBlock from "@components/DarkBlock/DarkBlock";
import Link from "next/link";
import Header from "@components/Header";
import { Banner } from "@components/Banner/Banner";

type MainProps = {
  actionsData: {
    title: string;
    bg: { url: string };
    actionField: { image: { url: string }; desscription: string }[];
  };
};

const Action: FC<MainProps> = ({ actionsData }) => {
  console.log(actionsData, "actionData");
  return (
    <div>
      <Header />
      <Banner
        title={actionsData.title}
        imgPath={`${api}${actionsData.bg.url}`}
      />
      <div className="container">
        {actionsData.actionField.map((item) => {
          return (
            <div className={"action-mb"}>
              <div
                key={item.image.url}
                className={"action-block"}
                style={{
                  backgroundImage: `url(${api}${item.image.url})`,
                }}
              />
              <div className="action-description">{item.desscription}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const actionsData = await fetcher(`${api}/actions`);
  return {
    props: {
      actionsData,
    },
  };
}

export default Action;
