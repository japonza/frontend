import React, { FC } from "react";
import { api } from "../src/constant/api";
import { fetcher } from "../src/helpers/fether";
import Header from "@components/Header";
import { Banner } from "@components/Banner/Banner";
import Card from "@components/Card/Card";

type MenuProps = {
  menu: {
    id: string;
    order: number;
    name: string;
    image: { url: string };
  }[];
};

const Menu: FC<MenuProps> = ({ menu }) => {
  return (
    <div>
      <Header />
      <Banner title={"Меню"} imgPath={"img/banner.png"} />
      <div className="container">
        <div className="row">
          {menu
            .sort((a, b) => a.order - b.order)
            .map((item, index) => {
              return (
                <Card
                  fire={false}
                  weight={null}
                  key={item.id}
                  id={item.id}
                  menu={true}
                  img={`${api}${item.image.url}`}
                  index={index}
                  priceTextBlock={item.name}
                  descriptionBlock={null}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const menu = await fetcher(`${api}/catalogs`);
  return {
    props: {
      menu,
    },
  };
}

export default Menu;
