import React from "react";
import { AppProps } from "next/app";

import "../node_modules/slick-carousel/slick/slick.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "@styles/global.scss";
import "@styles/required.scss";

import NProgress from "nprogress"; //nprogress module
import "nprogress/nprogress.css";
import { YMInitializer } from "react-yandex-metrika";

import Layout from "@components/Layout";
import { Router } from "next/router";
import ScrollToTop from "react-scroll-up";
import Image from "next/image";

import { ContextBugger, ManageBurger } from "../src/helpers/burgerContext";
import Head from "next/head";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Head>
        <title>Japonza - Доставка японской кухни</title>
        <meta name="author" content="Japonza" />
        <meta
          name="keywords"
          content="заказ роллы на дом доставка заказать Пенза сет наборы японская кухня еда заказ роллы на домдоставка роллов на домроллы заказдоставка ролловроллы доставкароллы заказатьроллы пензароллы сетдоставка роллов пензазаказ роллов на дом пензазаказ роллов пензазаказать доставку ролловроллы доставка пензароллы заказать пензароллы на дом пензароллы наборыроллы пенза доставкаяпонская кухня доставкаяпонская еда доставка"
        />
        <meta name="robots" content="index, follow" />
        <meta name="description" content="Japonza - Доставка японской кухни" />
        <meta
          name="description"
          content="Japonza предлагает свои услуги по доставке японской кухни. На нашем сайте вы можете заказать сеты роллов с различными видами начинки, которую вам доставят прямо на дом. Для получения более подробной информации по доставке наборов блюд японской кухни, обращайтесь по телефону в Пензе  8 (8412) 33-02-33."
        />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="copyright"
          content="Официальный сайт доставки японской кухни “Японза”"
        />
        <meta property="og:title" content="Japonza - Доставка японской кухни" />
        <meta name="yandex-verification" content="c2cb90b4c8468064" />
        <meta
          property="og:site_name"
          content="Japonza - Доставка японской кухни"
        />
        <meta property="og:description" content="Доставка японской кухни" />
        <meta property="og:type" content="profile" />
        <meta property="og:url" content="https://japonza.ru/menu" />
        <link rel="shortcut icon" href="/img/favicon.ico" />
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
        <meta
          property="og:image"
          content="https://japonza.ru/api/uploads/order_banner_bad242964b.png"
        />
      </Head>
      <ScrollToTop showUnder={160}>
        <div
          style={{
            height: "30px",
            width: "30px",
            borderRadius: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Image
            src={"/img/arrow-up.svg"}
            height={20}
            width={20}
            alt={"arrow-up"}
          />
        </div>
      </ScrollToTop>
      <YMInitializer
        accounts={[78166999, 95441302]}
        options={{ webvisor: true }}
        version="2"
      />
      <ContextBugger>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ContextBugger>
    </>
  );
}

export default MyApp;
