import React, { FC, useCallback, useEffect, useState } from "react";
import Header from "@components/Header";
import { Banner } from "@components/Banner/Banner";
import Card from "@components/Card/Card";
import { fetcher } from "../../src/helpers/fether";
import { api } from "../../src/constant/api";
import Image from "next/image";
import { useRouter } from "next/router";

type CatalogDetailProps = {
  rolls: {
    fire: boolean;
    background: { url: string };
    name: string;
    image: { url: string };
    rolls: any[];
  };
};

const COUNT = 12;

const CatalogDetail: FC<CatalogDetailProps> = ({ rolls }) => {
  const router = useRouter();
  const [page, setPage] = useState(1);
  const arrayByPage = [
    ...rolls.rolls.sort((a, b) => a.orderInCatalog - b.orderInCatalog),
  ].slice(COUNT * (page - 1), COUNT * page);

  const renderBg = () => {
    if (rolls.background) {
      return `${api}${rolls.background.url}`;
    }
    return "/img/banner.png";
  };

  const setCurrentPage = useCallback(
    (page) => {
      setPage(page);
    },
    [router]
  );
  return (
    <div>
      <Header />
      <Banner title={rolls.name} imgPath={renderBg()} />
      <div className="container">
        <div className="row">
          {arrayByPage
            .sort((a, b) => a.orderInCatalog - b.orderInCatalog)
            .map((item, index) => {
              return (
                <Card
                  key={item.id}
                  fire={Boolean(item.fire)}
                  id={index}
                  menu={false}
                  weight={item.weight}
                  img={`${api}${item.image.url}`}
                  index={index}
                  priceTextBlock={item.price}
                  descriptionBlock={{
                    title: item.title,
                    text: item.description,
                  }}
                />
              );
            })}
        </div>
        {rolls.rolls.length > COUNT && (
          <div className="row">
            <ul className="pagination w-100 justify-content-center">
              {Array(Math.ceil(rolls.rolls.length / COUNT))
                .fill(0)
                .map((_item, index) => {
                  return (
                    <li
                      key={String(index)}
                      className={"pagination-item"}
                      onClick={() => setCurrentPage(index + 1)}
                    >
                      <span className={`${index + 1 === page ? "active" : ""}`}>
                        {index + 1}
                      </span>
                    </li>
                  );
                })}
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};
export async function getServerSideProps({ params, preview = null }) {
  const rolls = await fetcher(`${api}/catalogs/${params.id}`);
  if (!rolls) {
  }

  return {
    props: {
      preview,
      rolls: {
        ...rolls,
        rolls: rolls.rolls.map((el) => ({
          ...el,
          orderInCatalog: el.orderInCatalog ? el.orderInCatalog : 99999,
        })),
      },
    },
  };
}
export default CatalogDetail;
