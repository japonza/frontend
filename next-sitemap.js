module.exports = {
  siteUrl: process.env.SITE_URL || "https://japonza.ru",
  changefreq: "daily",
  priority: 0.7,
  sitemapSize: 5000,
  generateRobotsTxt: true,
};
